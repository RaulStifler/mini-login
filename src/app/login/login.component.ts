import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  @Output() iniciandoSesion = new EventEmitter();
  datosUsuario: Object;
  constructor() {
    this.datosUsuario = {
      userName: '',
      password: ''
    }
  }

  iniciarSesion(){
    this.iniciandoSesion.emit(this.datosUsuario);
  }

}
