import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  usuarioLoggeado:any;
  iniciarSesion(datosUsuario){
    console.log(datosUsuario);
    this.usuarioLoggeado = datosUsuario;    
  }
  logOut(){
    this.usuarioLoggeado = null;
  }
}
